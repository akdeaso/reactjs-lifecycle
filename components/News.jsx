import React, { Component } from "react";
import axios from "axios";
import {
  Container,
  Row,
  Col,
  Card,
  Button,
  Form,
  Navbar,
  Nav,
} from "react-bootstrap";

const apiKey = import.meta.env.VITE_API_KEY;

class NewsApp extends Component {
  state = {
    searchKeyword: "",
    articles: [],
    isLoading: false,
  };

  componentDidMount() {
    this.fetchNews("trending");
  }

  fetchNews = async (searchKeyword) => {
    const url = `https://newsapi.org/v2/everything?q=${searchKeyword}&apiKey=${apiKey}`;

    try {
      const response = await axios.get(url);
      this.setState({ articles: response.data.articles, isLoading: false });
    } catch (error) {
      console.error("Error fetching news:", error);
      this.setState({ articles: [], isLoading: false });
    }
  };

  handleInputChange = (event) => {
    const searchKeyword = event.target.value;
    this.setState({ searchKeyword });

    if (searchKeyword.trim() === "") {
      this.fetchNews("trending");
    } else {
      this.setState({ isLoading: true });
      this.fetchNews(searchKeyword);
    }
  };

  render() {
    const { articles, isLoading } = this.state;

    return (
      <Container>
        <Navbar bg="light" expand="lg">
          <Navbar.Brand>News App</Navbar.Brand>
          <Navbar.Toggle aria-controls="basic-navbar-nav" />
          <Navbar.Collapse id="basic-navbar-nav">
            <Nav className="mr-auto"></Nav>
          </Navbar.Collapse>
        </Navbar>

        <Row className="my-5">
          <Col>
            <Form>
              <Form.Group controlId="searchInput">
                <Form.Control
                  type="text"
                  placeholder="Search news..."
                  value={this.state.searchKeyword}
                  onChange={this.handleInputChange}
                />
              </Form.Group>
            </Form>
          </Col>
        </Row>
        <Row>
          <Col>
            {isLoading ? (
              <div className="text-center">
                <div className="spinner-border" role="status">
                  <span className="sr-only"></span>
                </div>
              </div>
            ) : articles.length === 0 ? (
              <p>No news found.</p>
            ) : (
              <Row>
                {articles.map((article, index) => (
                  <Col md={4} key={index} className="mb-4">
                    <Card>
                      <Card.Img
                        variant="top"
                        src={
                          article.urlToImage ||
                          "https://via.placeholder.com/150"
                        }
                        style={{ height: "200px", objectFit: "cover" }}
                      />
                      <Card.Body>
                        <Card.Title>{article.title}</Card.Title>
                        <Card.Text>{article.description}</Card.Text>
                        <Button
                          href={article.url}
                          target="_blank"
                          variant="primary"
                        >
                          Read More
                        </Button>
                      </Card.Body>
                    </Card>
                  </Col>
                ))}
              </Row>
            )}
          </Col>
        </Row>
      </Container>
    );
  }
}

export default NewsApp;
